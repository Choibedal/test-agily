<?php

namespace App\Controller;

use App\Entity\Search;
use App\Form\SearchType;
use App\Service\WeatherService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\ItemInterface;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_main')]
    public function index(Request $request, WeatherService $weatherService): Response
    {
        // Create entity to generate form easily
        $search = new Search();
        $form = $this->createForm(SearchType::class, $search);

        // Handle form submission
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Check if coords exists in cache in order to redirect, create them if needed orreturn null if impossible, leading to error message
            $coords = $weatherService->getCoords($search->getName());
            if ($coords != null) {
                return $this->redirectToRoute('app_results', ["city" => $search->getName()]);
            } else {
                $form->addError(new FormError("La ville n'a pas pu être comprise"));
            }
        }

        return $this->renderForm('home/page1.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{city}', name: 'app_results')]
    public function result(Request $request, $city, WeatherService $weatherService): Response
    {
        // Check if coords exists in cache in order to redirect, create them if needed or return null if impossible, leading to error message
        $weather_data = $weatherService->getWeatherData($city);

        if ($weather_data != null) {
            return $this->render('home/page2.html.twig', [
                'today' => $weather_data[0],
                'days' => array_slice($weather_data,1)
            ]);
        }

        $this->addFlash("error", "Impossible de récupérer les données météo pour la ville de " . $city);
        return $this->redirectToRoute('app_main');

    }

    #[Route('/json/coords/{city}', name: 'app_json_coords')]
    public function coordsJSON(Request $request, $city, WeatherService $weatherService): Response
    {
        // Check if coords exists in cache in order to redirect, create them if needed or return null if impossible, leading to error message
        $coords = $weatherService->getCoords($city);
        if ($coords != null) {
            return new JsonResponse(["data" => json_encode($coords), "message" => "Succès"], Response::HTTP_OK);

        } else {
            return new JsonResponse(["data" => null, "message" => "Impossible d'identifier la ville choisie"], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    #[Route('/json/weather/{city}', name: 'app_json_weather')]
    public function weatherJSON(Request $request, $city, WeatherService $weatherService): Response
    {
        // Check if weather data exists in cache in order to redirect, create them if needed or return null if impossible, leading to error message
        $weather_data = $weatherService->getWeatherData($city);

        if ($weather_data != null) {
            return new JsonResponse(["data" => json_encode($weather_data), "message" => "Succès"], Response::HTTP_OK);

        } else {
            return new JsonResponse(["data" => null, "message" => "Une interne erreur est survenue"], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }




}
