<?php

namespace App\Service;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Contracts\Cache\ItemInterface;

class WeatherService
{
    /**
     * @var FilesystemAdapter
     */
    private $cache;

    private $owm_api_key;

    private $client;


    /**
     * Param is set in services.yml file to avoid presence in code
     * @param $owm_api_key
     */
    public function __construct($owm_api_key)
    {
        $this->cache = new FilesystemAdapter();
        $this->owm_api_key = $owm_api_key;
        $this->client = new CurlHttpClient();
    }

    /**
     * @param $city
     * @return mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getCoords($city)
    {
        return $this->cache->get($city, function (ItemInterface $item) use ($city) {
            $item->expiresAfter(3600);
            $url = "http://api.openweathermap.org/geo/1.0/direct?q=" . urlencode($city) . "&limit=5&appid=" . $this->owm_api_key;

            $response = $this->client->request('GET', $url);
            $content = json_decode($response->getContent());

            if (is_array($content) && isset($content[0]) && isset($content[0]->lat) && isset($content[0]->lon)) {
                $lat = $content[0]->lat;
                $long = $content[0]->lon;
                return ["lat" => $lat, "long" => $long];
            }

            return null;
        });

    }

    /**
     * @param $city
     * @return mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getWeatherData($city)
    {
        return $this->cache->get($city . ".weather_data", function (ItemInterface $item) use ($city) {
            $item->expiresAfter(1);
            $coords = $this->getCoords($city);
            if($coords != null) {
                $url = "https://api.openweathermap.org/data/2.5/onecall?lat=" . $coords["lat"] .
                    "&lon=" . $coords["long"] .
                    "&exclude=minutely,hourly" .
                    "&units=metric&appid=" . $this->owm_api_key;

                $client = new CurlHttpClient();
                $response = $client->request('GET', $url);
                $content = json_decode($response->getContent());

                $days = [];
                foreach ($content->daily as $day) {
                    $days[] = [
                        "date" => (new \DateTime())->setTimestamp($day->dt),
                        "day" => number_format($day->temp->day, 1),
                        "night" => number_format($day->temp->night, 1),
                        "pressure" => $day->pressure,
                        "wind" => $day->wind_speed,
                        "humidity" => $day->humidity,
                        "icon" => $day->weather[0]->icon
                    ];
                }
                return $days;
            }

            return null;
        });

    }
}
