# Test Technique Agily

## Consignes
Les consignes test technique sont les suivants :

Dans ton mon, la partie serveur devra être réalisée en Php / Symfony >= 4  (+ MariaDB si necessaire) en lieu et place de NodeJS/Express.

Les livrables minimum sont les suivants :

- Un nouveau repo gitlab ou github public, qui contiendra un readme.md mis a jour, les fichiers que tu vas écrire.
- Un ensemble de dossiers/fichiers selon une nomenclature claires
- Pour chacun des exercices, écrire le temps que tu auras mis à le réaliser au travers d'un README que tu essayeras de détailler (le markdown est recommandé), de préférences individuellement pour chaque partie


Il est important de respecter les consignes suivantes :

- Tu n’as pas besoin d'utiliser un service externe pour les exercices, sauf quand c'est explicitement demandé
- Il est très facile de trouver des solutions de ce test en ligne, aussi tu seras jugé sur ta capacité de structurer ton code
- Certaines consignes ne sont pas claires, tu devras trouver la solution par toi même

## Déroulement du test

Les exercices étaient un peu particuliers a adapter car conçus pour des technologies front. La structure même de l'architecture MVC encourage de fait le découplage et l'isolation du code pour pouvoir le réutiliser.
De plus, les principes des technologies front n'ont rien à voir avec celles du back, et il me semblait peu pertinent de prendre le temps de coder une interface unique qui ne fonctionnerait qu'avec des appels AJAX pour prendre en compte les interactions users (qui si c'est une méthode pertinent pour les technologies front, ce n'est clairement pas le but d'une techno comme symfony, encore moins sans utilisation de librairie externes telles que jQuery)

### Backend

Le livrable presente donc une solution codé en Symfony 6 puisque les consignes demandait un Symfony >= 4. J'ai essayé de le créer de base sur Symfony 4, mais ma machine n'avait pas les binaires requis pour la création d'un projet Symfony 4 et après 20 minutes infructueuses, j'ai choisi Symfony 6 qui n'est que très peu différent du 4 pour un projet de cette envergure.

Celle-ci est backée pour des raisons de praticité et de propreté du code par une base SQLite, nécessaire à la génération du formulaire bien qu'inutilisée. Celle-ci ne demande pas d'installation et me permet d'utiliser un fonctionnement proche de réels use-case. Celle-ci n'étant utilisée que pour générer le formulaire, elle aurait très bien pu être remplacé par du code en dur, mais j'ai voulu gardé l'esprit Symfony qu'on retrouve dans de plus gros projets. De plus, on pourrait tout à fait sauvegarder les villes recherchées à des fins statistiques par exemple. Ce choix est donc avant tout long-terme.

Le fonctionnement est simple : Lorsque la page1 est demandée, le serveur génère le formulaire et l'affiche. Lors de sa soumission, on vérifie en back que :
- le nom de la ville est reconnu par OpenWeatherMap
- les coordonnées d'une ville 

En effet, il y a très peu de chances que les coordonnées d'une ville changent d'une requête à une autre. La mise en cache via Symfony étant très intuitive, il n'y a aucun intérêt à ne pas la mettre en place à ce moment là.
Si la ville n'est pas reconnue, l'utilisateur est redirigé vers la page initiale avec un message d'erreur. Autrement, il est redirigé vers la page2 https://monsite.fr/Nom_de_la_ville (*ex: https://monsite.fr/Paris*) . L'utilisateur se voit donc afficher son résultat.

Les bonnes pratiques Symfony font que le code métier est situé dans les services. Créer une URL pour pouvoir rendre les données en JSON (une sorte d'adaptation de l'exercice 2) a donc été très rapide une fois que le code avait été écrit pour les controlleurs.

La rédaction totale de cette partie était d'environ 40 minutes, et comptait la mise en place du projet, la conception de l'architecture sur brouillon, la relecture des consignes, la lecture de l'API OWM et le développement de la solution et de ses endpoints JSON

### Frontend

N'ayant pas le droit d'importer de bibliothèques externes, j'ai tant bien que mal essayé pendant 1h20  environ de reproduire l'interface demandée mais sans aucun succès. Ayant toujours travaillé avec les bibliothèques Bootstrap et Tailwind, je n'ai tout simplement plus les bases en CSS pour reproduire une interface responsive et propre from scratch. J'ai donc tout effacé, et produit une interface simple, efficace, fonctionnelle et utilisable sur téléphone (bien que non responsive, dans l'absolu). 

J'avais au bout d'une trentaine de minute tenté de joindre le numéro de téléphone laissé dans le README de l'énoncé pour savoir si cette partie était vraiment pertinente et adaptée pour un projet Symfony, mais sans succès.

Après une pause et ne souhaitant pas louper le reste du projet pour réinventer la roue, j'ai donc rédigé un simple squelette en HTML / CSS et coder la mise à jour des cartes en Javascript natif en 30 minutes. 

Les 30 minutes restantes que je m'accordais pour finir en 2h30 ont servi à la rédaction du README et a l'upload sur le repo distant.
